from django.db import models
from uuid import uuid4

# Create your models here.

class Filmes(models.Model):
    id_filme = models.UUIDField(primary_key=True, default=uuid4, editable=False)
    titulo = models.CharField(max_length=225)
    ano =  models.IntegerField()
    genero = models.CharField(max_length=50)
    create_at = models.DateField(auto_now_add=True)

class Livros(models.Model):
    id_livro = models.UUIDField(primary_key=True, default=uuid4, editable=False)
    titulo = models.CharField(max_length=225)
    create_at = models.DateField(auto_now_add=True)