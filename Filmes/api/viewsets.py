from rest_framework import viewsets
from Filmes.api import serializers
from Filmes import models

# autenticação
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated

class FilmesViewset(viewsets.ModelViewSet):
    serializer_class = serializers.FilmesSerializer
    queryset = models.Filmes.objects.all()

# uma view protegida
class LivrosViewset(viewsets.ModelViewSet):
    permission_classes = [IsAuthenticated]
    authentication_classes = [TokenAuthentication]
    serializer_class = serializers.LivrosSerializer
    queryset = models.Livros.objects.all()