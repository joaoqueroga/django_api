from rest_framework import serializers
from Filmes import models


class FilmesSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Filmes
        fields = '__all__'

class LivrosSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Livros
        fields = '__all__'