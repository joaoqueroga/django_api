from django.db import models
from uuid import uuid4

class User(models.Model):
    id = models.AutoField(primary_key=True)
    nome = models.CharField(max_length=25)
    username = models.CharField(max_length=150, unique=True)
    password = models.CharField(max_length=128)

    def __str__(self):
        return self.nome

class Projetos(models.Model):
    id_projetos = models.UUIDField(primary_key=True, default=uuid4, editable=False)
    titulo = models.CharField(max_length=225)
    create_at = models.DateField(auto_now_add=True)

class Todo(models.Model):
   title = models.CharField(max_length=100)
   description = models.TextField()
   completed = models.BooleanField(default=False)

   def _str_(self):
     return self.title
