from django.apps import AppConfig


class ArcadequestionConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'arcadequestion'
