from rest_framework import serializers
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
from arcadequestion.models import User, Projetos

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = '__all__'

class ProjetosSerializer(serializers.ModelSerializer):
    class Meta:
        model = Projetos
        fields = '__all__'

class CustomTokenObtainPairSerializer(TokenObtainPairSerializer):
    # Implemente quaisquer personalizações adicionais aqui, se necessário
    pass
