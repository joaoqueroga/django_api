from rest_framework import viewsets
from arcadequestion.api import serializers
from arcadequestion.models import Projetos
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated

class ProjetosViewSet(viewsets.ModelViewSet):
    permission_classes = [IsAuthenticated]
    authentication_classes = [TokenAuthentication]    
    serializer_class = serializers.ProjetosSerializer
    queryset = Projetos.objects.all()
