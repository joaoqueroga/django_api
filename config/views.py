from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import AllowAny
from arcadequestion.models import Projetos, User
from arcadequestion.api.serializers import ProjetosSerializer, UserSerializer
from arcadequestion.api.serializers import CustomTokenObtainPairSerializer
from arcadequestion.api.serializers import UserSerializer
from arcadequestion.api import serializers
from rest_framework import viewsets
from rest_framework_simplejwt.views import TokenObtainPairView
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated

# Views
class UserCreateView(APIView):
    permission_classes = [AllowAny]

    def post(self, request):
        serializer = UserSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=201)
        return Response(serializer.errors, status=400)

class ProjetosViewSet(viewsets.ModelViewSet):
    permission_classes = [IsAuthenticated]
    authentication_classes = [TokenAuthentication]    
    serializer_class = ProjetosSerializer
    queryset = Projetos.objects.all()


class CustomTokenObtainPairView(TokenObtainPairView):
    serializer_class = CustomTokenObtainPairSerializer

    def post(self, request, *args, **kwargs):
        response = super().post(request, *args, **kwargs)
        token = response.data.get('access')
        if token:
            print('Token:', token)
        return response

class MeView(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request):
        serializer = UserSerializer(request.user)
        return Response(serializer.data)