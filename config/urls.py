from django.contrib import admin
from django.urls import path, include
from .views import UserCreateView
from rest_framework import routers
from arcadequestion.api.viewsets import ProjetosViewSet
from .views import UserCreateView, CustomTokenObtainPairView, MeView

router = routers.DefaultRouter()
router.register(r'projetos', ProjetosViewSet, basename='projetos')

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include(router.urls)),
    path('api/users/create/', UserCreateView.as_view(), name='user-create'),
    path('api/token/', CustomTokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('api/me/', MeView.as_view(), name='me'),
    
]
