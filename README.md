**Criação de APIs com Django Rest Framework**

João da Silva Queroga - 26 de Maio de 2023

*Criado no sistema operacional linux.*

**Passo 1:** criar a pasta de projeto.

```
$ sudo mkdir projeto_api
```

**Passo 2:** criar o ambiente virtulal para python 3 (usar 3.6 ou superior).
```
$ python3 -m venv env
```
**Passo 3:** ativar o ambiente.
```
$ source ./env/bin/activate
```
**Passo 4:** instalar o Django e o Rest Framework.
```
$ pip install django djangorestframework
```
**Passo 5:** Criar o projeto django (config).
```
$ django-admin startproject config .
```
**Passo 6:** criar o requirements (arquivo de módulos e dependencias).
```
$ pip freeze > requirements.txt
```
**Passo 7:** fazer as migrations no banco de dados (SQLite Padrão).
```
$ python manage.py migrate
```
*Observação: se quiser fazer com outro banco (Postgres, Mysql, …) instalar os módulos de conexão e configurar o arquivo settings.py.*

**Passo 8:** iniciar a aplicação.
```
$ python manage.py runserver 8080
```
**Criação da API**

**Passo 9:** Criar o APP
```
$ django-admin startapp Filmes
```

**Passo 10:** adicione o APP e o Rest Framework nas configurações em config/settings.py
```
INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'rest_framework',
    'Filmes'
]
```

**Passo 11:** Criar os models em Filmes/models.py 
```
from django.db import models
from uuid import uuid4

# Create your models here.

class Filmes(models.Model):
    id_filme = models.UUIDField(primary_key=True, default=uuid4, editable=False)
    titulo = models.CharField(max_length=225)
    ano =  models.IntegerField()
    genero = models.CharField(max_length=50)
    create_at = models.DateField(auto_now_add=True)
```
**Passo 12:** Criar os serializers e viewsets

- Criar a pasta api dentro da pasta Filme 
- Criar os arquivos serializers.py e viewsets.py dentro de api

serializers.py
```
from rest_framework import serializers
from Filmes import models


class LivrosSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Filmes
        fields = '__all__'
```
viewsets.py
```
from rest_framework import viewsets
from Filmes.api import serializers
from Filmes import models

class FilmesViewset(viewsets.ModelViewSet):
    serializer_class = serializers.FilmesSerializer
    queryset = models.Filmes.objects.all()
```

**Passo 13:** Configurar as rotas config/urls.py
```
from django.contrib import admin
from django.urls import path, include

from rest_framework import routers
route = routers.DefaultRouter()
from Filmes.api import viewsets as FilmesViewsets

route.register(r'filmes', FilmesViewsets.FilmesViewset , basename="Filmes")

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include(route.urls))
]
```
**Passo 14:** Fazer as migrations
```
$ python manage.py makemigrations
$ python manage.py migrate
```
**Passo 15:** Rodar a aplicação
```
$ python manage.py runserver 8080
```

—

**Outras configurações**

- CRIAR SUPER USUÁRIO PARA ADMIN
```
$ python manage.py createsuperuser
```
- CORS

**Passo 1:** instalar o cors
```
$ pip install django-cors-headers
$ pip freeze > requirements.txt
```
**Passo 2:** configurar o settings.py
```
MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'corsheaders.middleware.CorsMiddleware',
]

CORS_ALLOW_ALL_ORIGINS = True
CORS_ALLOW_CREDENTIALS = True

CORS_ALLOW_HEADERS = [
    "accept",
    "accept-encoding",
    "authorization",
    "content-type",
    "dnt",
    "origin",
    "user-agent",
    "x-csrftoken",
    "x-requested-with",
]
```

*Atenção: com estas configurações qualquer origem pode acessar sua API, configure corretamente quando subir para produção os campos CORS_ALLOW_ALL_ORIGINS e
CORS_ALLOW_CREDENTIALS.*

**Configurar o .env**

- criar um arquivo .env na raiz
- colar as configurações

```
POSTGRES_HOST=""
POSTGRES_PORT=""
POSTGRES_DB=""
POSTGRES_USER=""
POSTGRES_PASSWORD=""

SECRET_KEY=""

```
- instalr a bibliotaca python-dotenv

```
pip install python-dotenv
```
- importação
```
import os
from dotenv import load_dotenv
load_dotenv()
```
- exemplo de uso
```
SECRET_KEY = os.getenv('SECRET_KEY')
```

**Sistema de login**
***Criar Users***

- em ***congif/serializers.py***

```
from django.contrib.auth.models import User
from rest_framework import serializers

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['username', 'password']
        extra_kwargs = {'password': {'write_only': True}}

    def create(self, validated_data):
        user = User.objects.create_user(**validated_data)
        return user

```

- em ***congif/views.py***

```
from rest_framework import generics
from django.contrib.auth.models import User
from .serializers import UserSerializer

class UserCreateView(generics.CreateAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer

```

- em ***congif/urls.py***

```
from django.urls import path
from .views import UserCreateView

urlpatterns = [
    path('users/create/', UserCreateView.as_view(), name='user-create'),
    # outras URLs do seu projeto...
]
```

+ Agora, você pode enviar uma requisição POST para a URL users/create/ com os dados do usuário (por exemplo, { "username": "exemplo", "password": "senha" }) para cadastrar um novo usuário.

***Login JWT***

* Instalar o Json Web Token
```
pip install djangorestframework_simplejwt
```

- em **config/settings.py**

```
from datetime import timedelta

INSTALLED_APPS = [
    ...
    'rest_framework',
    'rest_framework_simplejwt',
    ...
]

REST_FRAMEWORK = {
    'DEFAULT_AUTHENTICATION_CLASSES': [
        'rest_framework_simplejwt.authentication.JWTAuthentication',
        'rest_framework.authentication.SessionAuthentication',
        'rest_framework.authentication.BasicAuthentication',
    ],
    'DEFAULT_PERMISSION_CLASSES': [
        'rest_framework.permissions.IsAuthenticated',
    ],
}

SIMPLE_JWT = {
    "ACCESS_TOKEN_LIFETIME": timedelta(minutes=60),
    "REFRESH_TOKEN_LIFETIME": timedelta(days=1),
}

```

- em ***congif/serializers.py***

```
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer

class CustomTokenObtainPairSerializer(TokenObtainPairSerializer):
    @classmethod
    def get_token(cls, user):
        token = super().get_token(user)
        
        # Adicione dados extras ao token, se necessário
        # token['meu_campo_extra'] = user.meu_campo_extra
        
        return token

```

- em ***congif/views.py***

```
from .serializers import CustomTokenObtainPairSerializer
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer

class CustomTokenObtainPairSerializer(TokenObtainPairSerializer):
    @classmethod
    def get_token(cls, user):
        token = super().get_token(user)
        
        # Adicione dados extras ao token, se necessário
        # token['meu_campo_extra'] = user.meu_campo_extra
        
        return token

```

- em ***config/urls.py***

```
from django.urls import path
from .views import CustomTokenObtainPairView

urlpatterns = [
    path('api/token/', CustomTokenObtainPairView.as_view(), name='token_obtain_pair'),
    # outras URLs do seu projeto...
]
```